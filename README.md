# Sudo Show 41 - Just the beginning

[Episode 41 Playback](https://sudo.show/41)

Brandon talks about plans and changes for the Sudo Show in 2022

Submit Show Ideas as a Github issue at https://gitlab.com/sudoshow/community

### Websites
- [Destination Linux Network](https://destinationlinux.network)
- [Sudo Show Website](https://sudo.show)

### Support the Show
- [Sponsor: Bitwarden](https://bitwarden.com/dln)
- [Sponsor: Digital Ocean](https://do.co/dln)
- [Sudo Show Swag](https://sudo.show/swag)

### Contact Us:
- [DLN Discourse](https://sudo.show/discuss)
- [Email Us!](mailto:contact@sudo.show)
- [Sudo Matrix Room](https://sudo.show/matrix)
- [Sudo Show GitLab](https://gitlab.com/sudoshow)

### Follow our Hosts:

- [Brandon's Website](https://open-tech.net)
- [Eric's Website](https://itguyeric.com)
- [Red Hat Streaming](https://www.redhat.com/en/livestreaming)

